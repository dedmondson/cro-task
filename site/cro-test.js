
jQuery(function() {

  // Find sections and swap
  let sectionsWrapper = jQuery("#main .elementor-section-wrap").get(0);
  let sections = jQuery(sectionsWrapper).children();
  let heroEl = sections.get(2);
  let ourWorkEl = sections.get(3);
  heroEl.before(ourWorkEl);

  // Insert the new button
  const contactButtonContainer  = jQuery(ourWorkEl).find(".ohio-recent-projects-sc");
  const contactButton = jQuery("\
     <button class='btn btn-default gint-button' >\
     Get in touch\
      <i class='ion-right ion ion-md-arrow-forward'></i>\
     </button>\
  ");
  contactButtonContainer.append(contactButton);

  // Add a new contact form dialog with masking overlay.
  let overlayEl = jQuery("\
  <div id='gint-overlay' style='display: none'  class='dialog-widget  dialog-lightbox-widget dialog-type-buttons dialog-type-lightbox elementor-popup-modal'>\
    <div class='overlay-content-wrapper' >\
    <div id='overlay-close-button' class='overlay_close'>\
      <i class='eicon-close' ></i>\
    </div>\
      <form id='gint-form' name='gint-form' action='.'>\
      <fieldset>\
        <legend>Just a few details and we'll be in touch.</legend>\
        <div class='field_wrapper' >\
              <label for='gint-firstname' >First name\
               <span class='required'>*</span>\
              </label>\
               <input id='gint-firstname' class='hs-input invalid error' type='text' name='firstname' required  autocomplete='given-name' >\
        </div>\
        <div class='field_wrapper' >\
              <label for='gint-lastname' >Last name\
               <span class='required'>*</span>\
              </label>\
              <input id='lastname' class='hs-input invalid error' type='text' name='lastname' required  autocomplete='family-name' >\
        </div>\
        <div class='field_wrapper' >\
              <label  for='gint-email' >Email\
               <span class='required'>*</span>\
              </label>\
               <input id='gint-email' class='hs-input invalid error' type='email' name='email' placeholder='Just so we can reply' required  autocomplete='email' >\
        </div>\
        <div class='field_wrapper' >\
              <label for='gint-company' >Company\
               <span class='required'>*</span>\
              </label>\
               <input id='gint-company'  class='hs-input invalid error' type='text' name='company' required placeholder='Who's it for? autocomplete='organization' >\
        </div>\
        <div class='field_wrapper' >\
              <label for='gint-phone' >Phone number\
               <span class='required'>*</span>\
              </label>\
               <input id='gint-phone' class='hs-input invalid error' type='tel' name='phone' required  placeholder='So someone can give you a call back' autocomplete='tel' >\
        </div>\
         <div class='field_wrapper' >\
              <label for='gint-message' >Your message\
               <span class='required'>*</span>\
              </label>\
               <textarea id='gint-message' class='hs-input invalid error'  name='message' required  placeholder='Can you tell us a bit about why you're getting in touch?  />\
        </div>\
         <div class='field_wrapper agreement' >\
              <input type='checkbox' id='gint-agreement' name='agreement' required >\
              <label for='gint-agreement'> Yes, I agree to Code Computerlove's <a href='.'>Privacy Policy</a>.<span class='required'>*</span></label><br>\
        </div>\
        <div class='actions' >\
          <button id='gint-submit-button' type='submit'  class='hs-button primary large' >Send message</button>\
        </div>\
      </fieldset>\
      </form>\
    </div>\
  </div>");
  jQuery("body").append(overlayEl);

  // Bind listeners
  jQuery( "button.gint-button" ).bind( "click", function() {
     showGintOverlay();
  });

  jQuery( "#overlay-close-button" ).bind( "click", function() {
     hideGintOverlay();
  });

  jQuery( "#gint-overlay" ).bind( "click", function(event) {
    if(event.target === this){
      hideGintOverlay();
    }
  });

  jQuery("#gint-submit-button").bind( "click", function() {
       jQuery( "#gint-form" ).addClass("validated");
  });
});

// Reusable functions
function showGintOverlay(){
	jQuery("#gint-overlay").css("display", "flex");
}

function hideGintOverlay(){
	jQuery("#gint-overlay").css("display", "none");
}
